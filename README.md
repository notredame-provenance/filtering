----- DEPENDENCIES -----

* This installation guide will be focused on Ubuntu 64bit.
Other versions of Linux can be used, provided the installation
commands are changed accordingly.

1. Install Linux 64 bit
2. Install Apache Web Server
 -> sudo apt-get install apache2 apache2-dev
 
3. Install required libraries: there are two methods
to install those libraries: via anaconda and manually.
Via anaconda is easier, but provides only python support.
Manually is harder, but provides support to C++ (Opencv)
as well. We recommend installing via anaconda

3.0 - Via Anaconda -
    3.0.1. Download Anaconda (http://continuum.io/downloads#all)
        -> wget https://3230d63b5fc54e62148e-c95ac804525aac4b6dba79b00b39d1d3.ssl.cf1.rackcdn.com/Anaconda-2.3.0-Linux-x86_64.sh
        
    3.0.2. Install Anaconda (command might vary 
           depending on the version downloaded)
           
        -> bash Anaconda-2.3.0-Linux-x86_64.sh
        
           Follow the steps for installation.
           When the installer asks to prepend
           Anaconda's install location to PATH,
           answer YES.
        
        -> Restart the system.
        
    3.0.3. Install anaconda packages used. Most
           of the required packages should be
           installed when Anaconda installs, but
           in case those aren't, the following
           packages should be installed:
           
        -> conda install opencv
        -> conda install numpy
        -> conda install flask
        -> conda install ipython
        -> conda install bitarray
        -> conda install lxml
        
           Anaconda should install all the
           dependencies needed, and update some
           packages.
    3.0.4. Create a new Anaconda environment for
           running the programs.
        -> conda create --name spotme opencv numpy flask ipython bitarray lxml
        
    3.0.5. Every time a program of the package has
           to be run, you need to activate the
           environment created. This is done with
           the following command:
           
        -> source activate <name_of_the_environment>
        
           in this example, <name_of_the_environment>
           is spotme. To deactivate the environment
           use:
           
        -> source deactivate
           
3.1 - Manual Install -
    3.1.1. Install python 2.7
        -> sudo apt-get install python2.7 python2.7-dev
        
    3.1.2. Install CMAKE
        -> sudo apt-get install cmake
        
    3.1.3. Install pip
        -> sudo apt-get install python-pip
        
    3.1.4. Install numpy
        -> sudo pip install numpy
        
    3.1.5. Install Flask
        -> sudo pip install flask
        
    3.1.6. Install lxml
        -> sudo pip install lxml
        
    3.1.2. Install Opencv (version >= 2.4.10 and < 3.0.0)
        -> sudo apt-get install build-essential
        -> sudo apt-get install git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
        -> sudo apt-get install libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev
        -> sudo apt-get install python-dev libxml2-dev libxslt1-dev zlib1g-dev
        -> wget https://github.com/Itseez/opencv/archive/2.4.11.zip
        -> unzip 2.4.11.zip
            -> If unzip installation is necessary: sudo apt-get install zip
        -> cd opencv-2.4.11
        -> mkdir release
        -> cd release
        -> cmake -DWITH_TBB=ON ..
        -> make
        -> sudo make install

4.0 Install FLANN
    For the new version of the application, pyflann is used to install pyflann, follow the next steps:
    
    4.0.1. If you installed anaconda, activate the environment installed.
    4.0.2. Download FLANN
        -> wget http://www.cs.ubc.ca/research/flann/uploads/FLANN/flann-1.9.1-src.zip
    4.0.3. Unzip the above package to the desired directory
        -> unzip flann-1.8.4-src.zip
    4.0.4. Install FLANN
        -> cd flann-1.8.4-src/
        -> mkdir release
        -> cd release
        -> cmake ..
        -> make
        -> make install
        
  I got it! I looked in here /usr/local/Cellar/flann/1.8.4/share/flann/python and saw a setup.py file. I enabled my virtualenv and then ran python setup.py install. It now works!
    It is very important to have the anaconda environment activated when installing. This way, the
    FLANN installation will recognize the anaconda python as the python distribution to look for.
    For convenience, the flann package is in the "dependencies" directory of this project.
  cmake -D BUILD_CUDA_LIB=ON -D CMAKE_INSTALL_PREFIX=/data/dasilvap/.local/flann-1.9.1 ../

----- PROJECT INSTALL -----

A single package is provided, organized in the followng directory tree:

    |spotme_dlv_final/
    |- dependencies/
    |- source/
      |- aux_files/
      |- database-tools/
      |- web-service/
      |- retrieval-tools/
      
The folder 'dependencies' contain the Anaconda and FLANN packages. The
folder 'aux_files' contains the groundtruth files for the unicamp and
oxford datasets. The folder 'web-service' contains the source code for
the retrieval web-service. The folder 'database-tools' contains the
scripts used to generate the database files used by the web-service.
Finally, the folder 'retrieval-tools' contains the tools used to test
and evaluate retrieval offline.
  
All tools are based on the python package "objret", which implements
various functions based on opencv and numpy for object retrieval.
Some of the files used by the retrieval web service are expected
to be acquired by using those tools.

To install, simply untar the packages in a directory of choice.

----- DATABASE SETUP -----

The scripts inside the 'database-tools' can be run to create the
necessary files (excluding groundtruth) to be used with the
web-service.

To run the retrieval web-service the following are required:
- Image Features
- Feature Indexing File
- Search Index
- Groundtruth File

A single script 'run_db_creation.py' is provided to produce
all the required files, except the groundtruth.

To run the script 'run_db_creation.py', a configuration file,
which we will refer as 'db_creation.conf' is required. This
configuration file has the following parameters:

###

db_creation.conf

[Directories]
imgdir=/home/alberto/SpotME/database_oxford5k/queries/queries_1.00/

[Feature Extraction]
detector=ORB
descriptor=ORB
feature_number=5000

[Index Creation]
index_type=LSH
distance_type=HAMMING

[Feature Selection]
selection_limit=500

###

- imgdir: directory containing the database images;

[Feature Extraction]
- detector: type of feature detector used;
            options:{"SURF", "SIFT", "ORB", "BRISK"}
- descriptor: type of feature descriptor used;
              options:{"SURF", "SIFT", "ORB", "RootSIFT", "BRISK"}
- feature_number: maximum number of features to be detected
                  by the detector;
                  
[Index Creation]
- index_type: type of FLANN index used;
              options:{"LINEAR", "KMEANS", "KDFOREST", "LSH", "HCAL", "AUTO"}
- distance_type: type of comparison masure used;
                 options:{"L1", "MANHATTAN", "L2", "EUCLIDEAN",  "HMD", "HAMMING",
                                 "CSQ", "CHISQUARE", "KBL", "KULLBACK-LEIBLER"}
                 *IMPORTANT*: some index type + distance combinations are incompatible.
                 
[Feature Selecton]
selection_limit: maximum number of features to
                 be selected using feature selection;
                 *IMPORTANT*: if selection_limit < 0,
                 then NO feature selection is performed.
                 The base features extracted in the first
                 step are used;
                 
A sample db_creation file is provided within te package.

'run_db_creation' script

usage: run_db_creation.py [-h] cfgfile outdir db_name

positional arguments:
  cfgfile     Path to the configuration file.
  outdir      Output directory.
  db_name     Name for the database. Will be used as prefix for the database
              generated files.
              
when run, this script will create the following directory structure:

|<outdir>/<db_name>/
|- features/
|- index/
|- rankfiles/
|- db_name.conf

The 'features' directory will containg the features file (.bfv), the keypoints
file (.bkp), and te feature indexing file (.txt). The 'index' directory will
contain the FLANN index file (.dat). Finally, the 'db_name.conf' is a config
file to be passed as argument to the web-service, to be discussed next. The
only empty argument in the config file is the 'groundtruth' path. This should
be provided by the user, depending on the dataset he is working with.

-- Groundtruth File --

The groundtruth file lists the classes that the database images belong
to. In the scenario of landmark recognition, this file will list the
database image and the building(s) contained on it. Consider, for example,
a dataset containing 5 different images, and 3 different landmarks. The
format of the groundtruth file is:

image_01.jpg landmark_02
image_03.jpg landmark_01
image_04.jpg landmark_01 landmark_03

Any images not listed are considered to not belong to any classes, or,
in this scenario, have any of the expected landmarks. An image might
contain more than one landmark. In the example above, image_02.jpg and
image_05.jpg do not contain any landmark.

The groundtruth file will be provided together with the datasets.

----- WEB-SERVICE SETUP -----

The web-service requires the four files listed above to work:

- Image Features FIle
- Feature Indexing File
- Search Index File
- Groundtruth File

The location of those files, as well as other configuration
parameters are parsed from a configuration file to be passed
as argument to the server initialization script. The format
of the configuration file is:

[Directories]
dbpath= <Root path containing the database files>
imdir= <Directory containing the database images>

[Folders]
featfolder= <Folder inside dbpath containing feature and
             feature indexing files>
gtfolder= <Folder containing groundtruth files>
indexfolder= <Folder containing FLANN index file>
imgfolder= <Folder containing database images>
rankings= <Output folder for ranking files>

[Files]
db_classes_file= <Name of the groundtruth file>
feat_file= <Name of the features file>
idx_file= <Name of the FLANN index file>
feat_index_file= <Name of the feature indexing file>

[Configuration]
detector = <Feature detector used in database creation>
descriptor = <Feature descriptor used in database creation>
feat_number = <Number of Local Features detected per image>
k = <Number of neighbors to be considered when performing search>

An example of the aforementioned file is given below:

[Directories]
dbpath=/home/database-SURF/

[Folders]
featfolder=features
gtfolder=groundtruth
indexfolder=index
imgfolder=images
rankings=rankfiles

[Files]
db_classes_file=db_classes.txt
feat_file=features_SURF.bin
idx_file=flann_index_SURF_KMEANS_L2.dat
feat_index_file=features_SURF_indexing.txt

[Configuration]
detector = SURF
descriptor = SURF
feat_number = 500
k = 10

For example, full path to features file will be:

/home/database-SURF/features/features_SURF.bin

The path building to the remaining files is similar.
Supposed this configuration file is named "Database-surf.conf",
To start the server, in the folder inside spotme_retrieval_web_service
containing the 'main.py' script, run:

usage: main.py [-h] config_file results_folder

positional arguments:
  config_file     Path to system configuration file.
  results_folder  Folder INSIDE static folder were the results are located.
  
  
----- OTHER TOOLS -----

In addition to the aforementioned scripts, another folder named 'retrieval-tools'
is available within the package. This folder contains several scripts used to
perform object retrieval offline and test our approaches. For more information,
check the documentation within each individual script.

     
----- REFERENCES -----

[1] Muja, Marius, and David G. Lowe. "Fast Approximate Nearest Neighbors
    with Automatic Algorithm Configuration." VISAPP (1) 2 (2009).

[2] http://docs.opencv.org/modules/flann/doc/flann_fast_approximate_nearest_neighbor_search.html

             